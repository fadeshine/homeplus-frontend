import React, { lazy } from 'react';
import { Route as RouterPage, Routes, Navigate } from 'react-router-dom';

const HomePage = lazy(() => import('../pages/HomePage'));
const ErrorPage = lazy(() => import('../pages/ErrorPage'));
const LoginPage = lazy(() => import('../pages/LoginPage'));
const RegisterPage = lazy(() => import('../pages/RegisterPage'));
const PostTaskPage = lazy(() => import('../pages/PostTaskPage'));
const DashboardPage = lazy(() => import('../pages/DashboardPage'));
const TaskDetailPage = lazy(() => import('../pages/TaskDetailPage'));
const BrowsePage = lazy(() => import('../pages/BrowsePage'));

const Route = () => (
  <Routes>
    <RouterPage path="/" element={<Navigate replace to="/home" />} />
    <RouterPage path="/login" element={<LoginPage />} />
    <RouterPage path="/register" element={<RegisterPage />} />
    <RouterPage path="/home" element={<HomePage />} />
    <RouterPage path="/post-task" element={<PostTaskPage />} />
    <RouterPage path="/task-detail" element={<TaskDetailPage />} />
    <RouterPage path="/dashboard" element={<DashboardPage />} />
    <RouterPage path="/browse" element={<BrowsePage />} />
    {/* <RouterPage element={<ProtectedRouterPage />}>
            <RouterPage
                path="/dashboard"
                element={
                    <MainLayout>
                        <DashboardPage />
                    </MainLayout>
                }
            />
        </RouterPage> */}
    <RouterPage path="*" element={<ErrorPage />} />
  </Routes>
);

export default Route;
