import styled from 'styled-components';

export const InfoCard = styled.div`
  h3,
  span {
    font-family: 'Roboto';
    font-weight: 300;
    color: #444;
  }
  svg {
    position: relative;
    left: 11%;
    width: 30px;
    transform: translateX(-50%);
    transform: scale(1.8);
  }
  display: flex;
  align-items: center;

  .info {
    text-align: center;
    span {
      font-size: 0.95rem;
      width: 50%;
    }
    .numbers {
      font-size: 1.3rem;
    }
    h3 {
      font-size: 0.8rem;
    }
  }
`;
