import React from 'react';

// import Icon from '@mui/material/Icon';
import Chip from '@mui/material/Chip';

import { InfoCard } from './informationCard.style';

const InformationCard = ({ icon, title, content }) => (
  <InfoCard>
    {icon ? (
      <div className="iconBox">
        <Chip
          icon={icon}
          sx={{
            width: '60px',
            height: '60px',
            top: '10px',
            position: 'relative',
            borderRadius: '50%',
            marginRight: '15px',
          }}
        />
      </div>
    ) : null}
    <div className="info">
      <h3>{title}</h3>
      <span className={icon ? '' : 'numbers'}>{content}</span>
    </div>
  </InfoCard>
);

export default InformationCard;
