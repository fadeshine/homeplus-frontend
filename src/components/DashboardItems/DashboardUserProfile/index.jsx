import React from 'react';
import {
  Wrapper,
  Left,
  PicArea,
  Pic,
  Username,
  GenderArea,
  LineWrapper,
  Labal,
  Value,
  Right,
  TextArea,
} from './DashboardUserProfile.style';

// TODO: Use props.gender, props.dob to replace static data later.
const DashboardUserProfile = () => (
  <Wrapper>
    <Left>
      <PicArea>
        <Pic />
        <Username>Username</Username>
      </PicArea>
      <GenderArea>
        <LineWrapper>
          <Labal>Gender</Labal>
          <Value>M</Value>
        </LineWrapper>
        <LineWrapper>
          <Labal>DOB</Labal>
          <Value>1996-01-07</Value>
        </LineWrapper>
      </GenderArea>
    </Left>
    <Right>
      <LineWrapper>
        <Labal>First Name</Labal>
        <Value>Victor</Value>
      </LineWrapper>
      <LineWrapper>
        <Labal>Last Name</Labal>
        <Value>Smith</Value>
      </LineWrapper>
      <LineWrapper>
        <Labal>Email</Labal>
        <Value>victor.smith@gmail.com</Value>
      </LineWrapper>
      <LineWrapper>
        <Labal>Address</Labal>
        <Value>Victoria St. 3000. VIC</Value>
      </LineWrapper>
      <LineWrapper>
        <Labal>Mobile</Labal>
        <Value>0466993254</Value>
      </LineWrapper>
      <LineWrapper>
        <Labal>Introduction</Labal>
      </LineWrapper>
      <TextArea>
        styled-components utilises tagged template literals to style your components. It removes the mapping between
        components and styles. This means that when youre defining your styles, youre actually creating a normal React
        component, that has your styles attached to it. This example creates two simple components, a wrapper and a
        title, with some styles attached to it:
      </TextArea>
    </Right>
  </Wrapper>
);

// DashboardUserProfile.propTypes = {
//   children: PropTypes.string.isRequired,
//   alertDot: PropTypes.bool,
// };

export default DashboardUserProfile;
