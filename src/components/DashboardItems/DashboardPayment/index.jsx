import React from 'react';
import { Box } from '@mui/system';
import Typography from '@mui/material/Typography';

const Payment = () => {
  return (
    <div>
      <Box
        sx={{
          ml: '20px',
          display: 'flex',
          justifyContent: 'left',
          color: '#444',
        }}
      >
        <dl>
          <dt>
            <Typography variant="h6" sx={{ fontWeight: '200' }}>
              Bank Account
            </Typography>
          </dt>
          <dd>
            <Typography sx={{ fontSize: '20' }}>
              <pre>BSB Number: {'   '}123 456</pre>
            </Typography>
            <Typography sx={{ fontSize: '20' }}>
              <pre>Account Number: {'   '}1234 5678</pre>
            </Typography>
          </dd>
        </dl>
      </Box>
    </div>
  );
};

export default Payment;
