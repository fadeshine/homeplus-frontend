import styled from 'styled-components';
export const Box = styled.div`
  width: 100%;
`;
export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  width: 80%;
  margin: 10% auto 10% auto;
`;
export const HomePageCardStyle = styled.div`
  flex: 2.5;
`;
